import analyser
a = analyser.Analyser()
a.load_data(label="multi", folder="outputs_multi")
a.load_data(label="single", folder="outputs_single")
a.load_data(label="multi_31", folder="outputs_multi_31")
a.load_data(label="multi_11", folder="outputs_multi_11")
a.load_data(label="multi_5", folder="outputs_multi_5")
a.load_data(label="multi_3", folder="outputs_multi_3")
a.load_data(label="multi_half", folder="outputs_multi_half")
a.load_data(label="multi_forth", folder="outputs_multi_forth")


idx_range = range(512, 3113)
specs = ["CO_DUST", "H_DUST"]
ybind = 600.
vmin = -5
vmax = 5
cmap = "RdBu_r"
clevels = [-4, -2, 0, 2, 4]
irads = [78, 82, 96]
jtimes = [41, 90]

klabs = ["51", "31", "11", "5", "3", "1"][::-1]
a.plot_flux_convergence(["multi", "multi_31", "multi_11", "multi_5", "multi_3", "single"][::-1],
                        {"multi": idx_range, "multi_31": range(392,1353),
                         "multi_11": range(272,393), "multi_5": range(236, 261),
                         "multi_3": range(224, 233), "single": [212]},
                        [82, 96], ["B", "C"], klabs, fname="plots/convergence.pdf")

klabs = ["$\sigma_{\\rm X}$", "$\sigma_{\\rm X}/2$", "$\sigma_{\\rm X}/4$", "$\sigma_{\\rm X}\\to0$"]
a.plot_flux_sigma(["multi", "multi_half", "multi_forth", "single"],
                        {"multi": idx_range, "multi_half": idx_range, "multi_forth": idx_range,
                         "single": [212]},
                        [82, 96], ["B", "C"], klabs, fname="plots/sigma.pdf")


sums = {"multi": idx_range,
        "single": [212]}

a.plot_flux_diff_rads(["multi", "single"], sums, irads, list("ABC"), jtimes=jtimes, ymin=None, ymax=None, fname="plots/flux_diff.pdf")


for irad in irads:
    for jtime in jtimes:
        fname = "plots/flux_map_%d_%d.svg" % (irad, jtime)
        print(fname)
        a.plot_flux_map(irad, jtime, idx_range, 212, specs, "multi", "single", ybind=ybind,
                        vmin=vmin, vmax=vmax, cmap=cmap, clevels=clevels, fname=fname)

