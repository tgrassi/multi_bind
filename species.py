

# *******************
# class species
class Species:

    # *****************
    # name: species name, eg CO2+
    def __init__(self, name):

        namefix = {"HE": "He",
                   "HE+": "He+",
                   "SI": "Si",
                   "SI+": "Si+",
                   "e-": "E",
                   "E-": "E"}

        # replace name to avoid problem with different species "spelling"
        if name in namefix:
            name = namefix[name]

        self.name = name
        self.fidx = None
        self.latex = None
        self.is_dust = False

        self.parse(name)
        self.set_latex()

    # *************
    # parse specie from name
    def parse(self, name):
        self.is_dust = "DUST" in name
        self.fidx = "idx_" + name.replace("+", "j").replace("-", "k")

    # *****************
    def set_latex(self):
        name = self.name
        for i in range(10):
            name = name.replace(str(i), "$_%d$" % i)
        name = name.replace("+", "$^+$")
        name = name.replace("-", "$^-$")
        if name == "E":
            name = "e$^-$"
        self.latex = name
