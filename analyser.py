from glob import glob
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import sys


class Analyser:

    # ***************
    def __init__(self):
        self.data = dict()
        self.data_flux = dict()
        self.ranges = dict()
        self.sigma = dict()
        self.label = None
        self.species = None
        self.binding_dict = dict()
        self.tdust_grid = dict()

        self.au2cm = 1.49598073e13  # au->cm
        self.spy = 365. * 24. * 3600.

        self.label_size = 16
        self.tick_label_size = 16
        self.legend_font_size = 14

    # ************************
    # load data from folder, data are output_[SPECIES].dat
    def load_data(self, label="ref", folder="outputs"):
        self.data[label] = dict()
        self.ranges[label] = dict()
        self.data_flux[label] = dict()

        if self.label is None:
            self.label = label

        print("Loading from %s with label %s" % (folder, label))

        # data is t, R (translated after loading)
        for fname in glob(folder + "/output_*.dat"):
            if "_flux_" in fname:
                iflux = fname.split("_")[-1]
                iflux = iflux.replace(".dat", "")
                self.data_flux[label][int(iflux)] = np.loadtxt(fname).T
            else:
                species = fname.split("/")[-1]
                species = species.replace("output_", "")
                species = species.replace(".dat", "")
                # data is [time, radius]
                self.data[label][species] = np.loadtxt(fname).T

                if "_DUST_" in species:
                    species_dust = species.split("_")[0] + "_DUST"
                    if species_dust not in self.data[label]:
                        self.data[label][species_dust] = np.zeros_like(self.data[label][species])
                    self.data[label][species_dust] += self.data[label][species]

        print("%d fluxes loaded in %s" % (len(self.data_flux[label]), label))

        self.ranges[label]["R"] = np.loadtxt(folder + "/range_r.dat") * self.au2cm
        self.ranges[label]["t"] = np.loadtxt(folder + "/range_t.dat")
        self.ranges[label]["ngas"] = np.loadtxt(folder + "/range_ngas.dat")
        self.tdust_grid[label] = np.loadtxt(folder + "/tdust_grid.dat")

        self.binding_dict[label] = np.load(folder + "/binding_dict.bin.npy", allow_pickle=True)

        # store species in a list when comparison
        self.species = self.data[label].keys()

    # ********************
    def plot_evolution_all(self, label="ref", only=None, ymin=None, ymax=None):
        for i, r in enumerate(self.ranges[label]["R"]):
            plt.clf()
            for species in self.data[label].keys():
                if only is not None:
                    if species not in only:
                        continue
                plt.loglog(self.ranges[label]["t"], self.data[label][species][:, i], label=species)
            plt.legend(loc="best", fontsize=5)
            fname = "plots/evolution_%.4f.png" % np.log10(r)
            plt.ylim(ymin=ymin, ymax=ymax)

            plt.xlabel("t/yr")
            plt.ylabel("n_i/n_tot")

            print(fname)
            plt.savefig(fname)

    # ********************
    def plot_evolution_diff(self, labels=None, only=None, ymin=None, ymax=None):

        if labels is None:
            labels = list(self.data.keys())

        if only is None:
            only = self.species

        colors = ["tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple",
                  "tab:brown", "tab:pink", "tab_gray", "tab:olive", "tab:cyan"]
        lss = ["-", "--", ":", "-."]
        mks = [None, None, "x"]

        for i, r in enumerate(self.ranges[labels[0]]["R"]):
            plt.clf()
            for k, label in enumerate(labels):
                for j, species in enumerate(only):

                    plt.loglog(self.ranges[label]["t"], self.data[label][species][:, i],
                               label="%s (%s)" % (species, label), color=colors[j],
                               ls=lss[k], marker=mks[k])

            plt.legend(loc="best", fontsize=6, ncol=len(labels))
            plt.title("%e" % np.log10(r))
            fname = "plots/diff_evolution_%.4f.png" % np.log10(r)
            plt.ylim(ymin=ymin, ymax=ymax)

            plt.xlabel("t/yr")
            plt.ylabel("n_i/n_tot")

            print(fname)
            plt.savefig(fname)

    # ********************
    def plot_radius_diff(self, labels=None, only=None, ymin=None, ymax=None):

        if labels is None:
            labels = list(self.data.keys())

        if only is None:
            only = self.species

        colors = ["tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple",
                  "tab:brown", "tab:pink", "tab_gray", "tab:olive", "tab:cyan"]
        lss = ["-", "--", ":", "-."]
        mks = [None, None, "x"]

        for i, t in enumerate(self.ranges[labels[0]]["t"]):
            plt.clf()
            for k, label in enumerate(labels):
                for j, species in enumerate(only):

                    plt.loglog(self.ranges[label]["R"], self.data[label][species][i, :],
                               label="%s (%s)" % (species, label), color=colors[j],
                               ls=lss[k], marker=mks[k])

            plt.legend(loc="best", fontsize=6, ncol=len(labels))
            plt.title("%e" % np.log10(t))
            fname = "plots/diff_radius_%.4f.png" % np.log10(t)
            plt.ylim(ymin=ymin, ymax=ymax)

            plt.xlabel("R/au")
            plt.ylabel("n_i/n_tot")

            print(fname)
            plt.savefig(fname)

    # *********************
    def set_label(self, label):
        self.label = label

    # *********************
    def get_label(self):
        return self.label

    # *************************
    # plot all the species
    def plot_all(self, folder="plots", vmin=None, vmax=None):
        # loop to plot all species
        for species in self.data[self.label].keys():
            self.plot_species(folder, species, vmin=vmin, vmax=vmax)

    # *************************
    # plot all the species
    def plot_diff(self, labels=None, folder="plots", vmin=None, vmax=None):

        if labels is None:
            labels = list(self.data.keys())

        if len(labels) != 2:
            sys.exit("ERROR: you need two labels for comparison!")

        # loop to plot all species
        for species in self.data[labels[0]].keys():

            # difference between bins is wrong (different energies)
            if "_DUST_" in species:
                continue

            val0 = self.data[labels[0]][species]
            val1 = self.data[labels[1]][species]
            val = (val0 + 1e-12) / (val1 + 1e-12)
            xval = self.ranges[labels[0]]["R"]
            yval = self.ranges[labels[0]]["t"]
            fname = folder + "/diff_%s.png" % species
            self.save_image(np.log10(xval), np.log10(yval), np.log10(val),
                            fname, "%s %s / %s" % (species, labels[0], labels[1]),
                            vmin=vmin, vmax=vmax)

    # *************************
    # plot single species
    def plot_species(self, folder, species, vmin=None, vmax=None):
        val = self.data[self.label][species]
        xval = self.ranges[self.label]["R"]
        yval = self.ranges[self.label]["t"]
        fname = folder + "/plot_%s.png" % species
        if vmin is not None:
            vmin = np.log10(vmin)
        if vmax is not None:
            vmax = np.log10(vmax)
        self.save_image(np.log10(xval), np.log10(yval), np.log10(val), fname, species,
                        vmin=vmin, vmax=vmax)

    # ****************************
    # generic save image to png function
    @staticmethod
    def save_image(xwhat, ywhat, what, fname, title, vmin=None, vmax=None):
        plt.clf()
        plt.pcolor(xwhat, ywhat, what, vmin=vmin, vmax=vmax)
        plt.xlabel("log(r/au)")
        plt.ylabel("log(t/yr)")
        plt.colorbar()
        plt.title(title)

        print("plotting %s..." % fname)
        plt.savefig(fname)

    # ***********************
    # sums = {"single": {"R1": [101, 102, 103], "R2": [5]},
    #         "multi": {"R1": [55], "R2": [44]}}
    def plot_flux_diff(self, labels, sums, ymin=None, ymax=None):

        colors = ["tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple",
                  "tab:brown", "tab:pink", "tab_gray", "tab:olive", "tab:cyan"]
        lss = ["-", "--", ":", "-."]

        label0 = list(self.ranges.keys())[0]
        for j, R in enumerate(self.ranges[label0]["R"]):
            plt.clf()
            for k, label in enumerate(labels):
                xdata = self.ranges[label]["t"]
                ilab = 0
                for rlab, idxs in sums[label].items():
                    ydata = np.zeros_like(xdata)
                    for i in idxs:
                        # t, R
                        ydata += self.data_flux[label][i][:, j]
                    plt.plot(np.log10(xdata), np.log10(ydata),
                             label="%s (%s)" % (rlab, label),
                             color=colors[ilab], ls=lss[k])
                    ilab += 1
                fname = "plots/plot_flux_%05d.png" % j
                print(fname)
                plt.ylim(ymin, ymax)
                plt.legend(loc="best")
                plt.title("R=%f log/cm" % np.log10(R))
                plt.xlabel("log(t/yr)")
                plt.ylabel("log(flux/[cm-3/s])")
                plt.savefig(fname)

    # ***********************
    # sums = {"single": [101, 102, 103],
    #         "multi": [44]}
    def plot_flux_diff_rads(self, labels, sums, jrads, jlabs, jtimes=None, ymin=None, ymax=None,
                            fname="plots/plot_flux_rad.png"):
        from matplotlib.ticker import AutoMinorLocator

        colors = ["tab:blue", "tab:orange", "tab:green", "tab:red", "tab:purple",
                  "tab:brown", "tab:pink", "tab_gray", "tab:olive", "tab:cyan"]
        lss = ["-", "--", ":", "-."]

        label0 = list(self.ranges.keys())[0]
        plt.clf()
        ilab = 0
        for j, R in enumerate(self.ranges[label0]["R"]):
            if j not in jrads:
                continue
            for k, label in enumerate(labels):
                xdata = self.ranges[label]["t"]
                ydata = np.zeros_like(xdata)
                for i in sums[label]:
                    # t, R
                    ydata += self.data_flux[label][i][:, j]
                plt.plot(np.log10(xdata), np.log10(ydata),
                         label="%s, %s" % (jlabs[ilab], label),
                         color=colors[ilab], ls=lss[k], lw=2)

                if jtimes is not None:
                    for i in jtimes:
                        t = np.log10(self.ranges[label0]["t"][i])
                        plt.scatter([t], np.log10(ydata[i]), c=colors[ilab])

            ilab += 1

        if jtimes is not None:
            for i in jtimes:
                t = np.log10(self.ranges[label0]["t"][i])
                plt.axvline(t, ls=":", alpha=0.6, color="k", zorder=-999)

        plt.ylim(ymin, ymax)
        plt.legend(loc="best", ncol=3, columnspacing=None)
        plt.xlabel("log($t$/yr)", fontsize=self.label_size)
        plt.ylabel("log($R_{\\rm H, CO}$ / [cm$^{-3}$ s$^{-1}$])", fontsize=self.label_size)
        plt.gca().xaxis.set_minor_locator(AutoMinorLocator(10))
        plt.gca().yaxis.set_minor_locator(AutoMinorLocator(5))
        plt.gca().tick_params(axis='both', which='major', labelsize=self.tick_label_size)
        plt.tight_layout()
        plt.savefig(fname)

    # *********************
    def plot_flux_diff_map(self, labels, sums):

        label0, label1 = labels
        xdata = self.ranges[label0]["t"]
        ydata = self.ranges[label0]["R"]
        zdata = dict()
        for rlab, idxs in sums[label0].items():
            plt.clf()
            for label in labels:
                zdata[label] = np.zeros((len(xdata), len(ydata)))
                for i in sums[label][rlab]:
                    zdata[label] += self.data_flux[label][i][:, :]
            plt.pcolor(np.log10(xdata), np.log10(ydata),
                       np.log10((zdata[label0] + 1e-40) / (zdata[label1] + 1e-40)))
            fname = "plots/plot_flux_map_%s.png" % rlab
            print(fname)
            plt.xlabel("log(t/yr)")
            plt.ylabel("log(r/cm)")
            plt.title("%s / %s" % (label0, label1))
            plt.colorbar()
            plt.savefig(fname)

    # *******************
    def plot_distribution_evolution(self, irad):
        label = self.label

        dust_species_base = list(set([x.split("_")[0] + "_DUST"
                                      for x in self.species if "_DUST_" in x]))

        cmap = matplotlib.cm.get_cmap('viridis')

        plt.clf()
        # t, R
        for dust_base in dust_species_base:
            for i, t in enumerate(self.ranges[label]["t"]):
                f = float(i) / len(self.ranges[label]["t"])
                ydata = []
                xdata = []
                for species in sorted(self.species):
                    if dust_base in species and "_DUST_" in species:
                        ydata.append(self.data[label][species][i, irad])
                        xdata.append(self.binding_dict[label].item().get(species))

                plt.semilogy(xdata, ydata, color=cmap(f))
            plt.xlabel("$T_{\\rm b}$")
            plt.ylabel("$n_i/n_{\\rm tot}$")
            plt.title(dust_base)

            tmin = np.log10(np.amin(self.ranges[label]["t"]))
            tmax = np.log10(np.amax(self.ranges[label]["t"]))
            norm = matplotlib.colors.Normalize(vmin=tmin, vmax=tmax)
            sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
            sm.set_array([])
            plt.colorbar(sm, ticks=np.linspace(tmin, tmax, 10)) #,
            #                         boundaries=np.arange(-0.05, 2.1, .1))

            plt.tight_layout()
            plt.show()

    # *******************
    def plot_distribution_map(self, irad):
        label = self.label

        dust_species_base = list(set([x.split("_")[0] + "_DUST"
                                      for x in self.species if "_DUST_" in x]))

        cmap = matplotlib.cm.get_cmap('viridis')

        n_times = len(self.ranges[label]["t"])
        n_bins = len([x for x in self.species if dust_species_base[0] in x and "_DUST_" in x])

        plt.clf()
        # t, R
        for dust_base in dust_species_base:
            zdata = np.zeros((n_bins, n_times))
            xdata = []
            jsp = 0
            for species in sorted(self.species):
                if dust_base in species and "_DUST_" in species:
                    zdata[jsp, :] = self.data[label][species][:, irad]
                    xdata.append(self.binding_dict[label].item().get(species))
                    jsp += 1
            xdata = np.array(xdata)
            ydata = self.ranges[label]["t"]
            plt.pcolor(xdata,
                       np.log10(ydata),
                       np.log10(zdata.T))
            plt.xlabel("$T_{\\rm b}$ / K")
            plt.ylabel("$t$ / s")
            plt.title(dust_base)
            plt.colorbar()

            plt.tight_layout()
            plt.show()

    # *******************
    def plot_flux_convergence(self, labels, sums, irads, jlabs, klabs, fname="plots/convergence.png",
                              ymin=None, ymax=None):
        from matplotlib.ticker import AutoMinorLocator

        cmap = matplotlib.cm.get_cmap('viridis')

        lss = ["-", "--", ":"]
        plt.clf()
        for j, irad in enumerate(irads):
            for k, label in enumerate(labels):
                xdata = self.ranges[label]["t"]
                ydata = np.zeros_like(self.data_flux[label][0][:, irad])
                for i in sums[label]:
                    ydata += self.data_flux[label][i][:, irad]
                nk = len(labels) - 1
                plt.plot(np.log10(xdata), np.log10(ydata), label="%s, %s" % (jlabs[j], klabs[k]),
                         ls=lss[j], lw=2, color=cmap(float(k)/nk))

        plt.legend(loc="best",  ncol=len(irads), fontsize=self.legend_font_size)
        plt.xlabel("log($t$/yr)", fontsize=self.label_size)
        plt.ylabel("log($R_{\\rm H, CO}$ / [cm$^{-3}$ s$^{-1}$])", fontsize=self.label_size)
        plt.gca().xaxis.set_minor_locator(AutoMinorLocator(10))
        plt.gca().yaxis.set_minor_locator(AutoMinorLocator(5))
        plt.gca().tick_params(axis='both', which='major', labelsize=self.tick_label_size)
        plt.ylim(ymin, ymax)
        plt.tight_layout()
        plt.savefig(fname)

    # *******************
    def plot_flux_sigma(self, labels, sums, irads, jlabs, klabs, fname="plots/sigma.png",
                              ymin=None, ymax=None):
        from matplotlib.ticker import AutoMinorLocator

        lss = ["-", ":", "-.", "--"]
        colors = ["tab:orange", "tab:green"]

        plt.clf()
        for j, irad in enumerate(irads):
            for k, label in enumerate(labels):
                xdata = self.ranges[label]["t"]
                ydata = np.zeros_like(self.data_flux[label][0][:, irad])
                for i in sums[label]:
                    ydata += self.data_flux[label][i][:, irad]
                plt.plot(np.log10(xdata), np.log10(ydata), label="%s, %s" % (jlabs[j], klabs[k]),
                         ls=lss[k], lw=2, color=colors[j])

        plt.legend(loc="best",  ncol=len(irads), fontsize=self.legend_font_size-2)
        plt.xlabel("log($t$/yr)", fontsize=self.label_size)
        plt.ylabel("log($R_{\\rm H, CO}$ / [cm$^{-3}$ s$^{-1}$])", fontsize=self.label_size)
        plt.gca().xaxis.set_minor_locator(AutoMinorLocator(10))
        plt.gca().yaxis.set_minor_locator(AutoMinorLocator(5))
        plt.gca().tick_params(axis='both', which='major', labelsize=self.tick_label_size)
        plt.ylim(ymin, ymax)
        plt.tight_layout()
        plt.savefig(fname)

    # *******************
    def plot_flux_map(self, irad, jtime, idx_rates, idx_ref, species_list, label, label_ref,
                      vmin=None, vmax=None, vmin_from_vmax=False, xbind=None, ybind=None,
                      cmap="viridis", clevels=None, fname="plots/flux_map.png"):

        from matplotlib.ticker import AutoMinorLocator

        species_avail = list(self.data[label].keys())
        dust_species_base = list(set([x.split("_")[0] + "_DUST"
                                      for x in species_avail if "_DUST_" in x]))

        n_bins = len([x for x in species_avail if dust_species_base[0] in x and "_DUST_" in x])

        # t, R
        zdata = np.zeros((n_bins, n_bins))

        imin = min(idx_rates)
        for i in idx_rates:
            zdata[(i - imin) // n_bins, (i - imin) % n_bins] = \
                self.data_flux[label][i][jtime, irad]

        xdata = []
        ydata = []
        for species in sorted(species_avail):
            tbind = self.binding_dict[label].item().get(species)
            if species_list[0] in species and "_DUST_" in species:
                xdata.append(tbind)
            if species_list[1] in species and "_DUST_" in species:
                ydata.append(tbind)

        xdata = np.array(xdata)
        ydata = np.array(ydata)

        if xbind is None:
            ixbind = len(xdata) // 2
            xbind = xdata[ixbind]
        if ybind is None:
            iybind = len(ydata) // 2
            ybind = ydata[iybind]

        znorm = self.data_flux[label_ref][idx_ref][jtime, irad]

        zdata = np.log10((zdata / znorm).T)

        if vmin_from_vmax:
            vmax = np.amax(zdata)
            vmin = -vmax

        plt.clf()
        pc = plt.pcolor(xdata,
                        ydata,
                        zdata,
                        vmin=vmin,
                        vmax=vmax,
                        cmap=cmap,
                        linewidth=0,
                        rasterized=True)
        pc.set_edgecolor('face')

        cb = plt.colorbar()
        cb.ax.minorticks_on()

        if clevels is not None:
            cs = plt.contour(xdata,
                             ydata,
                             zdata, levels=clevels, colors="k", linestyles=":", alpha=0.7)
            plt.clabel(cs, inline=1, fontsize=14, fmt="%d")

        plt.xlabel("$T_{{\\rm b},j}$ (%s)" % species_list[0].replace("_DUST", ""),
                   fontsize=self.label_size)
        plt.ylabel("$T_{{\\rm b},i}$ (%s)" % species_list[1].replace("_DUST", ""),
                   fontsize=self.label_size)

        plt.scatter([xbind], [ybind], color="#262626", marker="x", s=60)

        plt.gca().xaxis.set_minor_locator(AutoMinorLocator(2))
        plt.gca().yaxis.set_minor_locator(AutoMinorLocator(2))
        plt.gca().tick_params(axis='both', which='major', labelsize=self.tick_label_size)

        tdust = self.tdust_grid[label][jtime, irad]
        time = self.ranges[label]["t"][jtime]
        radius = self.ranges[label]["R"][irad] / self.au2cm
        tbox = "$T_{\\rm d}=%.1f$ K\n$t=%s$ yr\n" \
               "$r=%s$ au\n$R_{\\rm ref}=%s$" % (tdust,
                                                 self.latex_float(time),
                                                 self.latex_float(radius),
                                                 self.latex_float(znorm))
        t = plt.text(xdata[1], ydata[1], tbox,
                     color="k", ha="left", va="bottom", fontsize=14)
        t.set_bbox(dict(facecolor='w', alpha=0.5, edgecolor=None))

        plt.tight_layout()
        plt.savefig(fname)

    # **********************
    @staticmethod
    def latex_float(f):
        float_str = "{0:.2g}".format(f)
        if "e" in float_str:
            base, exponent = float_str.split("e")
            latex = r"{0} \times 10^{{{1}}}".format(base, int(exponent))
            if latex.startswith("1 \\times"):
                latex = latex.replace("1 \\times", "")
            return latex
        else:
            return float_str
