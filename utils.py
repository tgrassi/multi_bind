import numpy as np
from scipy.interpolate import interp2d
from commons import idx_H, idx_H2, idx_Hj, idx_Hk
from commons import get_bind, get_imass_sqrt, get_mass_array


def init_co_shielding():
    print("CO loaded!")
    data_co = np.loadtxt("data/shield_CO.dat").T

    xvals = np.unique(np.log10(data_co[0]))
    yvals = np.unique(np.log10(data_co[1]))
    return interp2d(xvals, yvals, np.log10(data_co[2]))


f_co_shield = init_co_shielding()


# get H nuclei (simplified)
def get_hnuclei(n):
    return 2e0 * n[idx_H2] + n[idx_H] + n[idx_Hj] + n[idx_Hk]


def get_rhogas(n):
    mass = get_mass_array()

    # total gas density, g/cm3
    return np.sum(n[:] * mass[:])


# H3O attenuation function
def fhnoj(av):
    if av <= 15.:
        return np.exp(-2.55 * av + 0.0165 * av**2)
    else:
        return np.exp(-2.8 * av)


# H2 phodissociation self shielding
def fselfh2(ncol, b):
    x = ncol * 2e-15  # normalized column density (#)
    b5 = b * 1e-5  # normalized doppler broadening (#)

    return 0.965e0 / (1e0 + x / b5)**2 + \
           0.035 / np.sqrt(1e0 + x) * \
           np.exp(max(-8.5e-4 * np.sqrt(1e0 + x), -250.))


# dust sticking
def dust_stick(tgas, tdust):

    tgas100 = tgas * 1e-2
    tdust100 = tdust * 1e-2
    return 1e0 / (1e0 + 0.4e0 * np.sqrt(tgas100 + tdust100)
                  + 0.2 * tgas100 + 0.08 * tgas100**2)


# sticking rate
def krate_stick(n, idx, tgas, tdust, amin, amax, pexp, rho0, d2g):

    # get inverse mass squared
    imass = get_imass_sqrt(idx)
    # get masses
    mass = get_mass_array()
    # derived exponents
    p3 = pexp + 3.
    p4 = pexp + 4.

    # total dust density, g/cm3
    rhod = np.sum(n[:] * mass[:]) * d2g

    boltzmann_erg = 1.380648e-16
    pre_kvgas_sqrt = np.sqrt(8e0 * boltzmann_erg / np.pi)

    # compute rate (1/s) coefficient assuming normalization
    return pre_kvgas_sqrt * np.sqrt(tgas) * imass \
         * rhod / (4. / 3. * rho0) * p4 / p3 \
         * (amax**p3 - amin**p3) / (amax**p4 - amin**p4) \
         * dust_stick(tgas, tdust)


# simplified sticking rate for standard silicate grain distribution
def krate_stickSi(n, idx, tgas, tdust):
    amin = 5e-7  # cm
    amax = 2.5e-5  # cm
    pexp = -3.5
    rho0 = 3e0  # g/cm3
    d2g = 1e-2

    return krate_stick(n, idx, tgas, tdust, amin, amax, pexp, rho0, d2g)


# evaporation rate
def krate_evaporation(n, idx, tdust):

    nu0 = 1e12  # 1/s
    ebind = get_bind(idx)

    return nu0 * np.exp(-ebind / tdust)


def krate_surface(n, idx1, idx2, tbarrier, tdust):
    amin = 5e-7  # cm
    amax = 2.5e-5  # cm
    pexp = -3.5
    rho0 = 3e0  # g/cm3
    d2g = 1e-2
    app2 = 9e-16  # cm2
    atun = 1e-8  # cm
    hbar = 1.05457266e-27
    nu0 = 1e12  # 1/s
    fhop = 2e0 / 3e0
    kb = 1.380648e-16  # erg/K

    # get masses
    mass = get_mass_array()

    p3 = pexp + 3.
    p4 = pexp + 4.

    # total dust density, g/cm3
    rhod = np.sum(n[:] * mass[:]) * d2g

    ndns = 3e0 * rhod * (amax**p3 - amin**p3) * p4 / (amax**p4 - amin**p4) / p3 / rho0 / app2

    mred = 1e0 / (1e0 / mass[idx1] + 1e0 / mass[idx2])

    ptunnel = np.exp(-2e0 * atun / hbar * np.sqrt(2e0 * mred * kb * tbarrier))

    ebind1 = get_bind(idx1)
    ebind2 = get_bind(idx2)

    hop = np.exp(-fhop * ebind1 / tdust) + np.exp(-fhop * ebind2 / tdust)

    return ptunnel * nu0 * hop / ndns
