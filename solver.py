import sys


# ******************
# chemical solver
class Solver:

    # ****************
    # network: network object
    def __init__(self, network):
        from commons import argdict
        import scipy
        ver = [int(x) for x in scipy.__version__.split(".")]
        ver = 10000 * ver[0] + 100 * ver[1] + ver[2]
        if ver < 10104:
            print("ERROR: you need at least scipy 1.4.1")
            print("found " + scipy.__version__)
            sys.exit()

        self.network = network
        self.argdict = argdict

    # *******************
    # solve
    # n: initial conditions chemical species
    # tgas: gas temperature
    # t: np array with time steps
    def run(self, n, tgas, tdust, t, rtol=1e-8, atol=1e-12):
        from fex import fex
        from jex import jex
        from scipy.integrate import solve_ivp
        from rates import get_rates

        k = get_rates(n, tgas, tdust, self.argdict)

        # solve
        res = solve_ivp(fex, (min(t), max(t)), n,
                        t_eval=t, args=(tgas, tdust, k),
                        rtol=rtol, atol=atol, method="BDF",
                        jac=jex, jac_sparsity=None)

        if not res.success:
            print("ERROR:", res.message)
            sys.exit()

        return res.y.T

    # *****************
    # set user arguments
    def set_user_arg(self, strvar, val):
        if strvar not in self.argdict:
            print("ERROR: unknown user argument %s" % strvar)
            print("Available arguments are:")
            print(self.argdict.keys())
            sys.exit()
        self.argdict[strvar] = val

    # *******************
    def get_fluxes(self, n, tgas, tdust):
        from fex import get_fluxes
        from rates import get_rates

        k = get_rates(n, tgas, tdust, self.argdict)

        return get_fluxes(n, k)
