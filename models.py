from glob import glob
import numpy as np
from scipy.interpolate import RectBivariateSpline
import sys
import matplotlib.pyplot as plt


# load Tdust from models
# data structure is
# mdot
#  |---rc
#       |---ngas
#            |---t
#            |---rc
#            |---ngas
#            |---fTd
class Models:

    # ***************
    def __init__(self, folder="models/"):

        self.data = None
        self.load(folder)
        self.au2cm = 1.49598073e13

    # ********************
    def get_ftdust(self, mdot, rc, ngas, plot_info=True, irads=None, jtimes=None):

        if mdot not in self.data:
            print("ERROR: possible mdot values are:")
            print(self.data.keys())
            sys.exit()

        if rc not in self.data[mdot]:
            print("ERROR: possible rc values are:")
            print(self.data[mdot].keys())
            sys.exit()

        if ngas not in self.data[mdot][rc]:
            print("ERROR: possible ngas values are:")
            print(self.data[mdot][rc].keys())
            sys.exit()

        av = self.get_av(self.data[mdot][rc][ngas])

        if plot_info:
            self.plot_tdust(mdot, rc, ngas, irads=irads, jtimes=jtimes)

        return (self.data[mdot][rc][ngas]["t"],
                self.data[mdot][rc][ngas]["R"],
                av,
                self.data[mdot][rc][ngas]["fTd"],
                self.data[mdot][rc][ngas]["Tdust"],
                self.data[mdot][rc][ngas]["n(R)"])

    # ********************
    def plot_tdust(self, mdot, rc, ngas, irads=None, jtimes=None):
        from matplotlib.ticker import AutoMinorLocator

        # PLOT TGAS MAP
        xdata = self.data[mdot][rc][ngas]["R"]
        ydata = self.data[mdot][rc][ngas]["t"]
        zdata = self.data[mdot][rc][ngas]["Tdust"]
        plt.clf()
        pc = plt.pcolor(np.log10(xdata),  # * self.au2cm
                        np.log10(ydata),
                        np.log10(zdata),
                        cmap="coolwarm",
                        linewidth=0e0,
                        vmin=1e0,
                        vmax=3e0,
                        rasterized=True)
        pc.set_edgecolor('face')

        plt.colorbar()

        #plt.clf()

        temps = [2e1, 3e1, 5e1, 1e2, 3e2, 5e2]  # , 1e3, 2e3]
        cs = plt.contour(np.log10(xdata),
                         np.log10(ydata),
                         zdata, levels=temps, colors="k", linestyles="--")
        plt.clabel(cs, inline=1, fontsize=12, fmt="%d K")

        labs = list("ABCDEFGHIJKLMNOPQRSTUVWXYZ")
        cols = ["tab:blue", "tab:orange", "tab:green"]
        if irads is not None:
            for j, ir in enumerate(irads):
                xline = np.log10(xdata)[ir]
                ytext = 5.
                plt.axvline(xline, color=cols[j], lw=3)
                plt.text(xline, ytext, labs[j], ha="center", size=16)
                if jtimes is not None:
                    for i, jt in enumerate(jtimes):
                        yline = np.log10(ydata)[jt]
                        plt.scatter([xline], [yline], c=cols[j], s=60)

        plt.gca().xaxis.set_minor_locator(AutoMinorLocator())
        plt.gca().yaxis.set_minor_locator(AutoMinorLocator())

        plt.xlabel("log($r$ / au)", fontsize=14)
        plt.ylabel("log($t$ / yr)", fontsize=14)
        plt.gca().tick_params(axis='both', which='major', labelsize=14)
        plt.xlim(left=1.5)
        plt.tight_layout()
        plt.savefig("plots_info/tdust_map.svg")

        # PLOT TGAS PROFILE
        plt.clf()
        for i, r in enumerate(xdata):
            if i % 5 != 0:
                continue
            label = "%.2f log/cm" % np.log10(xdata[i] * self.au2cm)
            if np.amin(zdata[:, i]) > 5e2:
                continue
            plt.semilogx(xdata, zdata[:, i], label=label)
        plt.ylim(0, 5e2)

        plt.legend(loc="best", ncol=2, fontsize=10)
        plt.savefig("plots_info/tdust_profiles.png")

        # PLOT TGAS FIT MAP
        nx = 100
        ny = 50

        xmin = np.amin(xdata)
        xmax = np.amax(xdata)
        xdata = np.logspace(np.log10(xmin), np.log10(xmax), nx)
        ymin = np.amin(ydata)
        ymax = np.amax(ydata)
        ydata = np.logspace(np.log10(ymin), np.log10(ymax), ny)
        zdata = np.zeros((nx, ny))
        for i in range(nx):
            for j in range(ny):
                zdata[i, j] = self.data[mdot][rc][ngas]["fTd"].ev(xdata[i], ydata[j])

        plt.clf()
        plt.pcolor(np.log10(xdata * self.au2cm),
                   np.log10(ydata),
                   np.log10(zdata).T)
        plt.xlabel("log(R/au)")
        plt.ylabel("log(t/yr)")
        plt.colorbar()
        plt.savefig("plots_info/tdust_fit.png")

        # PLOT Av
        plt.clf()
        plt.semilogy(np.log10(self.data[mdot][rc][ngas]["R"] * self.au2cm),
                     self.get_av(self.data[mdot][rc][ngas]))
        plt.xlabel("log(R/cm)")
        plt.ylabel("Av")
        plt.savefig("plots_info/av_profile.png")

        # PLOT NGAS
        plt.clf()
        plt.semilogy(np.log10(self.data[mdot][rc][ngas]["R"] * self.au2cm),
                     self.data[mdot][rc][ngas]["n(R)"])
        plt.xlabel("log(R/cm)")
        plt.ylabel("ntot/cm3")
        plt.savefig("plots_info/ngas_profile.png")

    # *******************
    @staticmethod
    def get_av(data):

        au2cm = 1.49598073e13
        N2Av = 1.87e21

        av = np.zeros_like(data["R"])

        for i, r in enumerate(data["R"][1:-1]):
            av_up = 1e99  # FIXME np.trapz(data["n(R)"][:i+1], data["R"][:i+1]) * au2cm / N2Av
            av_down = np.trapz(data["n(R)"][i:], data["R"][i:]) * au2cm / N2Av
            av[i] = min(av_up, av_down)

        av[:] = 1e99  # FIXME

        av[0] = av[1]
        av[-1] = av[-2]

        return av

    # ********************
    # load data
    def load(self, folder):

        data = dict()
        icount = None  # to trigger error if not declared

        # loop on Mdot folders
        for d1 in glob(folder + "/*"):
            # get Mdot
            mdot = d1.split("/")[-1]
            data[mdot] = dict()

            # loop on models folder
            for d2 in glob(d1 + "/*"):

                # get metadata from filename, Rc, ngas
                _, _, rc, ngas = d2.split("/")[-1].split("_")
                if rc not in data[mdot]:
                    data[mdot][rc] = dict()

                # prepare data
                data[mdot][rc][ngas] = {"R": None,
                                        "t": None,
                                        "n(R)": None,
                                        "Tdust": None,
                                        "fTd": None}

                in_tdust = False
                # loop on file to read
                for row in open(d2 + "/nT_table.txt"):
                    srow = row.strip()
                    if srow == "":
                        continue

                    arow = srow.split(" ")

                    # if reading Tdust data
                    if in_tdust:
                        data[mdot][rc][ngas]["Tdust"][icount, :] = \
                            np.array([float(x) for x in arow])
                        icount += 1
                    else:
                        # row starting with Tdust initialize reading Tdust data
                        if srow.startswith("Tdust"):
                            in_tdust = True
                            icount = 0
                            nt = len(data[mdot][rc][ngas]["t"])
                            nr = len(data[mdot][rc][ngas]["R"])
                            data[mdot][rc][ngas]["Tdust"] = np.zeros((nr, nt))
                            continue
                        else:
                            # read R, t, n(R), first element is name
                            data[mdot][rc][ngas][arow[0]] = np.array([float(x) for x in arow[1:]])

                # interpolator for later use
                data[mdot][rc][ngas]["fTd"] = RectBivariateSpline(data[mdot][rc][ngas]["R"],
                                                                  data[mdot][rc][ngas]["t"],
                                                                  data[mdot][rc][ngas]["Tdust"].T,
                                                                  s=0)
                data[mdot][rc][ngas]["Tdust"] = data[mdot][rc][ngas]["Tdust"].T

        # copy to attribute
        self.data = data
