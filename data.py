import numpy as np


# *******************
class Data:

    # *******************
    def __init__(self, nbins_bind=100, binding_data="data/binding.dat"):

        self.binding = dict()
        self.binding_prob = dict()
        self.atoms = dict()
        self.species_mass = dict()
        self.weighted_rate = dict()

        self.load_atoms("data/atoms.dat", nbins_bind)
        self.load_binding(binding_data, nbins_bind)

    # *******************
    def load_atoms(self, fname, nbins_bind):
        print("Loading %s..." % fname)

        for row in open(fname):
            srow = row.strip().replace("\t", " ")
            if srow == "" or srow.startswith("#"):
                continue

            atom, mass = [x for x in srow.split(" ") if x.strip() != ""]

            self.atoms[atom] = float(mass)

        for i in range(nbins_bind):
            atom = "_DUST_%03d" % i
            self.atoms[atom] = 0e0

    # *****************
    @staticmethod
    def fbind(x, x0, s):
        return np.exp(-(x - x0)**2 / 2. / s**2)

    # *******************
    def load_binding(self, fname, nbins_bind):
        import matplotlib.pyplot as plt

        print("Loading %s..." % fname)

        plt.clf()
        for row in open(fname):
            srow = row.strip()
            if srow == "" or srow.startswith("#"):
                continue

            # read row: species, tbind, sigma, weight, ...
            arow = [x for x in srow.split(" ") if x.strip() != ""]

            # species name
            species = arow[0]
            # gaussian functions data
            agauss = arow[1:]

            # init limits binding energy
            bmin = 1e99
            bmax = -1e99
            # limits binding energy
            bmin_lim = 1e2
            bmax_lim = 1e99

            # value limit for P(Tbind) gaussian
            plim = 1e-4

            # loop on gaussians to find limits
            for i in range(len(agauss) // 3):
                tbind = float(agauss[i*3])
                sigma = float(agauss[i*3+1])

                bmin = min(bmin, tbind - np.sqrt(-2.*sigma**2*np.log(plim)))
                bmax = max(bmax, tbind + np.sqrt(-2.*sigma**2*np.log(plim)))
                bmin = max(bmin, bmin_lim)
                bmax = min(bmax, bmax_lim)

            ps = np.zeros(nbins_bind)
            bs = np.linspace(bmin, bmax, nbins_bind)
            # loop on gaussians to sum
            for i in range(len(agauss) // 3):
                tbind = float(agauss[i*3])  # central binding, K
                sigma = float(agauss[i*3+1])  # sigma gaussian, K
                wei = float(agauss[i*3+2])  # gaussian weight

                # sum gaussian to total
                ps += wei * self.fbind(bs, tbind, sigma)

            print(species, np.sum(ps*bs) / np.sum(ps))

            # normalize total gaussian
            ps /= np.sum(ps)

            # plot gaussians to check
            sp_label = species
            for ii in range(10):
                sp_label = sp_label.replace(str(ii), "$_%d$" % ii)

            plt.loglog(bs, ps, label=sp_label, lw=2)
            #for i in range(len(bs)):
            #    plt.text(bs[i], ps[i], i, ha="center", va="bottom")
            #plt.scatter(bs, ps, marker=".")
            #plt.title(species)
            #plt.xlabel("$T_{\\rm bind}$/K")
            #plt.ylabel("$P(T_{\\rm bind})$")
            #fname_plot = "plots_info/pbind_%s.png" % species
            #print("saving P(Tbind) to " + fname_plot)

            # add bindings to binding dictionary
            for i in range(nbins_bind):
                sp = species + "_DUST_%03d" % i
                self.binding[sp] = bs[i]
                self.binding_prob[sp] = ps[i]

        plt.xlabel("$T_{\\rm b, X}$", fontsize=16)
        plt.ylabel("$P(T_{\\rm b, X})$", fontsize=16)
        plt.gca().tick_params(axis='both', which='major', labelsize=16)
        plt.legend(loc="best", fontsize=14)
        plt.tight_layout()
        plt.savefig("plots_info/pbind_all.svg")
