import numpy as np
import sys
from reaction import Reaction
from data import Data


# convert f90 string to py (NOTE: this is NOT the f2py library!)
def f2py(arg):

    if "idx_" not in arg:
        arg = arg.lower()

    arg = arg.replace("sqrt(", "np.sqrt(")
    arg = arg.replace("log(", "np.log(")
    arg = arg.replace("exp(", "np.exp(")
    arg = arg.replace("log10(", "np.log10(")
    for i in range(10):
        arg = arg.replace("d%d" % i, "e%d" % i)
    arg = arg.replace("d-", "e-")
    return arg


# true if it is a number
def is_number(arg):
    try:
        float(arg)
        return True
    except ValueError:
        return False


def print_toast():
    print("*********************************************************")
    for row in open("data/toast.dat"):
        print(row.rstrip())
    print("               IT WILL TAKE SOME TIME!")
    print("   YOU CAN PREPARE A TOAST (OR TWO) IN THE MEANWHILE")
    print("*********************************************************")


# *******************
# class chemical network
class Network:
    # *******************
    # fname: name of the network file
    def __init__(self, fname, nbins_bind=100, binding_data="data/binding.dat"):
        self.commons = []
        self.vars = []
        self.reactions = []
        self.species_index = None
        self.species_names = None
        self.nbins_bind = nbins_bind
        self.multi_species = []

        self.data = Data(nbins_bind, binding_data=binding_data)

        # read network file
        self.read(fname)
        self.set_species_index()
        self.nmols = len(self.species_index)

        self.compute_species_mass()

        # preprocess files
        self.preprocessor_commons()
        self.preprocessor_rates()
        self.preprocessor_fex()
        self.preprocessor_jex()
        self.preprocessor_sparsity()

    # ******************
    # read chemical network in fname and
    def read(self, fname):

        print("Reading %s..." % fname)
        fmt = None  # defined to trigger error

        idx = 0
        for row in open(fname):
            srow = row.strip()
            if srow == "" or srow.startswith("#"):
                continue

            if srow.startswith("@var:"):
                vs = srow.split(":", 1)[1].lower().strip()
                vspy = f2py(vs)
                # check that @var is not used more than once
                for vsp in self.vars:
                    if vsp.split("=")[0].strip() == vspy.split("=")[0].strip():
                        print("ERROR: variable %s already found! Change name!" % vs)
                        sys.exit()
                self.vars.append(vspy)
                continue

            # add multispecies, e.g. CO_DUST, H2O_DUST
            if srow.startswith("@multi:"):
                multi_species = srow.split(":")[1].split(",")
                multi_species = [x.strip() for x in multi_species]
                if not all(["_DUST" in x for x in multi_species]):
                    print("ERROR: multi species must be _DUST species, found", multi_species)
                    sys.exit()
                self.multi_species += multi_species
                continue

            if srow.startswith("@format:"):
                fmt = srow.split(":")[1]
                continue

            if srow.startswith("@common:"):
                commons = srow.split(":")[1].split(",")
                self.commons += [x.lower().strip() for x in commons]
                continue

            if srow.startswith("@ice:"):
                fmt = "idx,R,P,rate"
                for i in range(self.nbins_bind):
                    srow_split = self.srow2bins(srow, i)
                    self.reactions.append(Reaction(srow_split, fmt, idx))
                    idx += 1
                continue

            if srow.startswith("@ice2:"):
                fmt = "idx,R,R,P,rate"
                for i in range(self.nbins_bind):
                    for j in range(self.nbins_bind):
                        srow_split = self.srow2bins2(srow, i, j)
                        self.reactions.append(Reaction(srow_split, fmt, idx, dry=True))
                        idx += 1
                continue

            if srow.startswith("@"):
                continue

            self.reactions.append(Reaction(srow, fmt, idx))
            idx += 1

    # ************************
    @staticmethod
    def species_replace(rate, species_dust_multi):
        spl = species_dust_multi.split("_")
        sp = spl[0]
        parts = []
        for s in rate.split(","):
            if s.strip() == "idx_" + sp:
                s = "idx_" + species_dust_multi.upper()
            parts.append(s)

        return ",".join(parts)

    # ************************
    def srow2bins2(self, srow, jdx1, jdx2):
        r1, r2, p1, rate = srow.replace("@ice2:", "").split(",", 3)
        species_dust1 = "%s_%03d" % (r1, jdx1)
        species_dust2 = "%s_%03d" % (r2, jdx2)

        rate = self.species_replace(rate, species_dust1)
        rate = self.species_replace(rate, species_dust2)
        return "0, %s, %s, %s, %s" % (species_dust1, species_dust2, p1, rate)

    # ************************
    def srow2bins(self, srow, jdx):
        species, mode, rate = srow.replace("@ice:", "").split(",", 2)

        species_dust = "%s_DUST_%03d" % (species, jdx)

        if mode == "freezeout":
            rate += "* %e" % self.data.binding_prob[species_dust]
            return "0, %s, %s, %s" % (species, species_dust, rate)
        elif mode == "evaporation":
            rate = rate.replace(species, species_dust)
            return "0, %s, %s, %s" % (species_dust, species, rate)
        else:
            print("ERROR: unknown mode " + mode)
            print(srow)
            sys.exit()

    # ************************
    def compute_species_mass(self):
        from itertools import product
        parts = sorted(self.data.atoms.keys(), key=lambda x: len(x))[::-1]
        reps = ["".join(x) for x in list(product('WXYZ', repeat=4))]

        for name in self.species_names:
            namecp = name
            for i, p in enumerate(parts):
                namecp = namecp.replace(p, "#%s#" % reps[i])
            namecp = namecp.replace("##", "#")
            ename = [x for x in namecp.split("#") if x != ""]

            mlast = 0
            mtot = 0e0
            for p in ename:
                if is_number(p):
                    mtot += mlast * (int(p) - 1)
                else:
                    idx = parts[reps.index(p)]
                    mlast = self.data.atoms[idx]
                    mtot += mlast

            proton_mass = 1.6726219e-24  # g
            self.data.species_mass[name] = mtot * proton_mass

    # ****************
    def network2latex_file(self, fname):

        fout = open(fname, "w")
        rows = []
        for reaction in self.reactions:
            if reaction.is_dust:
                continue
            row = reaction.get_latex_table_row()
            if row in rows:
                continue
            rows.append(row)
        tab_head = "\\begin{tabular}{%s}\n" % ("l"*(rows[0].count("&")+2))
        tab_head += "\\hline\n"

        tab_foot = "\\hline\n"
        tab_foot += "\\end{tabular}\n"

        fout.write(tab_head)
        for i, row in enumerate(rows):
            fout.write("%d & %s\\\\\n" % (i+1, row))
            if (i+1) % 60 == 0:
                fout.write(tab_foot)
                fout.write("\n")
                fout.write(tab_head)

        fout.write(tab_foot)
        fout.close()

    # *****************
    # loop to get the list of the available species
    def set_species_index(self):

        species = []
        names = []
        for r in self.reactions:
            for s in r.reactants + r.products:
                if s.fidx not in species:
                    species.append(s.fidx)
                    names.append(s.name)

        self.species_index = species
        self.species_names = names

    # **********************
    def get_index(self, fidx):
        return self.species_index.index(fidx)

    # **********************
    def get_binding_dict(self):
        return self.data.binding

    # *************************
    # generic file preprocessor
    @staticmethod
    def preprocessor(fname, body_in):
        print("writing %s..." % fname)

        body = "################################################\n"
        body += "#  THIS FILE IS AUTOGENERATED! DO NOT EDIT!\n"
        body += "#      EVERY CHANGE WILL BE WIPED OUT!\n"
        body += "################################################\n"

        body += body_in

        # write to file
        fout = open(fname, "w")
        fout.write(body)
        fout.close()

    # *****************
    # write commons.py file
    def preprocessor_commons(self, fname="commons.py"):

        body = "import numpy as np\n"
        body += "from scipy.interpolate import interp2d\n\n"

        # loop to get species indexes, e.g. idx_H2 = 3
        for i, s in enumerate(self.species_index):
            body += "%s = %i\n" % (s, i)

        body += "\n"

        # variables for dust bin limits
        species_found = []
        for s in self.species_index:
            if "_DUST_" in s:
                spec = s.split("_DUST_")[0]
                if spec in species_found:
                    continue
                body += spec + "_DUST_MIN = " + spec + "_DUST_%03d\n" % 0
                body += spec + "_DUST_MAX = " + spec + "_DUST_%03d\n" % (self.nbins_bind - 1)
                species_found.append(spec)

        # argument dictionary, e.g. argdict{"user_av": 0e0}
        body += "\n"
        body += "argdict = {%s}" % ", ".join(["\"%s\": 0e0" % x for x in self.commons])
        body += "\n\n"

        # binding energies, get_bind(idx) function
        # where idx is the integer corresponding to the species
        # if binding is missing use 0e0
        dbind = self.data.binding
        lbind = []
        for i, s in enumerate(self.species_names):
            if s in dbind:
                lbind.append(dbind[s])
            else:
                lbind.append(0e0)

        body += "\ndef get_bind(idx):\n"
        body += "\treturn [" + ",\n\t\t".join(["%e" % x for x in lbind]) + "][idx]\n\n"

        mass = self.data.species_mass
        lmass = []
        for i, s in enumerate(self.species_names):
            lmass.append(mass[s])

        body += "\ndef get_mass(idx):\n"
        body += "\treturn [" + ",\n\t\t".join(["%e" % x for x in lmass]) + "][idx]\n\n"

        body += "\ndef get_imass_sqrt(idx):\n"
        body += "\treturn [" + ",\n\t\t".join(["%e" % (1e0 / np.sqrt(x)) for x in lmass]) \
                + "][idx]\n\n"

        body += "\ndef get_mass_array():\n"
        body += "\treturn np.array([" + ",\n\t\t".join(["%e" % x for x in lmass]) + "])\n\n"

        body += "\ndef get_name(idx):\n"
        body += "\treturn [" + ",\n\t\t".join(["\"" + x + "\"" for x in self.species_names]) \
                + "][idx]\n\n"

        # trange = self.data.weighted_rate["trange"]
        # rhorange = self.data.weighted_rate["rhorange"]
        # trange_s = "[%s]" % ",".join(["%e" % x for x in trange])
        # rhorange_s = "[%s]" % ",".join(["%e" % x for x in rhorange])
        # for k, v in self.data.weighted_rate.items():
        #     if "DUST" in k:
        #         wrange_s = "[%s]" % ",".join(["%e" % x for x in v])
        #         wrate = "krate_evaporation_weighted_%s = interp2d(%s, %s, %s)\n" % (k, rhorange_s, trange_s, wrange_s)
        #         body += wrate.lower()

        self.preprocessor(fname, body)

    # *****************
    # write rates.py file
    def preprocessor_rates(self, fname="rates.py"):

        body = "import numpy as np\n"
        body += "from commons import *\n"
        body += "from utils import get_hnuclei, fhnoj, krate_stickSi\n"
        body += "from utils import krate_surface\n"
        body += "from utils import krate_evaporation, fselfh2, f_co_shield, get_rhogas\n\n"

        # for k, v in self.data.weighted_rate.items():
        #     if "DUST" in k:
        #         body += "from commons import krate_evaporation_weighted_%s\n" % k.lower()

        body += "def get_rates(n, tgas, tdust, args):\n"
        body += "\tk = np.zeros(%d)\n\n" % len(self.reactions)

        for c in self.commons:
            body += "\t%s = args[\"%s\"]\n" % (c, c)

        # prepare @var variables
        for v in self.vars:
            body += "\t%s\n" % v
        body += "\n"

        # loop to write reactions, e.g. k[3] = 1.5e-6 * np.sqrt(tgas)
        for r in self.reactions:
            body += "\t# %s\n" % r.get_verbatim()
            if r.has_tlimits():
                body += "\tif tgas >= %e and tgas < %e:\n\t" % (r.tmin, r.tmax)
            body += "\tk[%d] = %s\n\n" % (r.idx, r.rate)

        # body += "\n\tprint(np.argmin(k), np.min(k))\n\n"

        body += "\n\treturn k\n\n"

        self.preprocessor(fname, body)

    # *****************
    # write fex.py file
    def preprocessor_fex(self, fname="fex.py"):

        body = "import numpy as np\n"
        body += "from commons import *\n\n"

        body += "def get_fluxes(n, k):\n"
        body += "\tflux = np.zeros(%d)\n\n" % len(self.reactions)

        # fluxes, e.g. flux[22] = k[22]*n[idx_H2]*n[idx_C]
        for r in self.reactions:
            body += "\tflux[%d] = %s\n" % (r.idx, r.rhs)

        body += "\treturn flux\n\n"

        body += "def fex(t, n, tgas, tdust, k):\n"
        body += "\tdn = np.zeros_like(n)\n\n"
        body += "\tflux = get_fluxes(n, k)\n\n"

        # prepare ODE, i.e. sum fluxes to products, subtract fluxes to reactants
        ode = dict()
        for rea in self.reactions:
            # dry reactions do not affect abundances
            if rea.dry:
                continue
            for r in rea.reactants:
                if r.fidx not in ode:
                    ode[r.fidx] = []
                ode[r.fidx].append("\\\n\t\t-flux[%d]" % rea.idx)
            for p in rea.products:
                if p.fidx not in ode:
                    ode[p.fidx] = []
                ode[p.fidx].append("\\\n\t\t+flux[%d]" % rea.idx)

        # write RHS, e.g. dn[idx_CH] = -flux[idx_CH] + flux[idx_C] + ...
        for k, v in ode.items():
            body += "\tdn[%s] = %s\n" % (k, " ".join(v))

        body += "\n\treturn dn\n\n"

        self.preprocessor(fname, body)

    # *****************
    # write fex.py file
    def preprocessor_jex(self, fname="jex.py"):

        body = "import numpy as np\n"
        body += "from commons import *\n\n"

        body += "def jex(t, n, tgas, tdust, k):\n"
        body += "\tdjac = np.zeros((len(n), len(n)))\n\n"

        # prepare JAC, i.e. sum fluxes to products, subtract fluxes to reactants
        jac = np.full((self.nmols, self.nmols), "", dtype=object)
        for i1, s1 in enumerate(self.species_index):
            for i2, s2 in enumerate(self.species_index):
                if jac[i1, i2] == "":
                    jac[i1, i2] = "djac[%s, %s] = " % (s1, s2)

        for r in self.reactions:
            # dry reactions do not affect abundances
            if r.dry:
                continue
            # jacobian derivatives
            for dr in [x.fidx for x in r.reactants]:
                dflux = r.get_flux_derivative(dr)
                i2 = self.get_index(dr)
                for rdx in [x.fidx for x in r.reactants]:
                    i1 = self.get_index(rdx)
                    jac[i1, i2] += " \\\n\t\t-" + dflux
                for pdx in [x.fidx for x in r.products]:
                    i1 = self.get_index(pdx)
                    jac[i1, i2] += " \\\n\t\t+" + dflux

        for i1, s1 in enumerate(self.species_index):
            for i2, s2 in enumerate(self.species_index):
                if jac[i1, i2].endswith("= "):
                    continue
                body += "\t" + jac[i1, i2] + "\n"

        body += "\n\treturn djac\n\n"

        self.preprocessor(fname, body)

    # *****************
    # write fex.py file
    def preprocessor_sparsity(self, fname="sparsity.py"):

        body = "import numpy as np\n"
        body += "from commons import *\n\n"

        body += "def get_sparsity(n):\n"
        body += "\tspa = np.zeros((len(n), len(n)))\n\n"

        # prepare JAC, i.e. sum fluxes to products, subtract fluxes to reactants
        spa = np.full((self.nmols, self.nmols), "", dtype=object)

        for r in self.reactions:
            if r.dry:
                continue
            for dr in [x.fidx for x in r.reactants]:
                i2 = self.get_index(dr)
                for rdx in [x.fidx for x in r.reactants]:
                    i1 = self.get_index(rdx)
                    spa[i1, i2] = "spa[%s, %s] = 1e0" % (rdx, dr)
                for pdx in [x.fidx for x in r.products]:
                    i1 = self.get_index(pdx)
                    spa[i1, i2] = "spa[%s, %s] = 1e0" % (pdx, dr)

        for i1, s1 in enumerate(self.species_index):
            for i2, s2 in enumerate(self.species_index):
                if spa[i1, i2] == "":
                    continue
                body += "\t" + spa[i1, i2] + "\n"

        body += "\n\treturn spa\n\n"

        self.preprocessor(fname, body)
