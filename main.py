from network import Network
from solver import Solver
from models import Models
import matplotlib.pyplot as plt
from tqdm import tqdm
import sys
import os
import numpy as np

# load background models
mod = Models()

# get a specific model
t_range, r_range, av_range, f_tdust, Tdust_grid, ngas_range = mod.get_ftdust("Mdot3",
                                                                             "Rc5.0",
                                                                             "n5.0",
                                                                             irads=[78, 82, 96],
                                                                             jtimes=[41, 90])

# default folder to trig error
output_folder = None
if len(sys.argv) == 2:
    mode = sys.argv[1]
    output_folder = "outputs_" + mode
elif len(sys.argv) == 3:
    mode = sys.argv[1]
    output_folder = sys.argv[2]
else:
    print("ERROR: usage is")
    print("    python3 main.py [mode]")
    print("or")
    print("    python3 main.py [mode] [output folder]")
    sys.exit()

# create folders if missing
for folder in [output_folder, "plots", "plot_info"]:
    if not os.path.exists(folder):
        os.mkdir(folder)

# tmin = np.amin(t_range)
# tmax = np.amax(t_range)
# t_range = np.logspace(np.log10(tmin), np.log10(tmax), 500)

# max tdust temperature to run the code, K
# (to limit useless calculation with no surface chemistry)
tdust_lim = 2e2

# check number of grid points
if len(r_range) != len(ngas_range):
    sys.exit("ERROR: r and ngas range lengths are different!")

# create network object for different modes
if mode == "effective":
    net = Network("networks/react_CO_ice_effective", nbins_bind=1, binding_data="data/binding.dat")
elif mode == "single":
    net = Network("networks/react_CO_ice", nbins_bind=1, binding_data="data/binding_single.dat")
elif mode == "multi":
    net = Network("networks/react_CO_ice", nbins_bind=51, binding_data="data/binding.dat")
elif mode == "multi_31":
    net = Network("networks/react_CO_ice", nbins_bind=31, binding_data="data/binding.dat")
elif mode == "multi_21":
    net = Network("networks/react_CO_ice", nbins_bind=21, binding_data="data/binding.dat")
elif mode == "multi_11":
    net = Network("networks/react_CO_ice", nbins_bind=11, binding_data="data/binding.dat")
elif mode == "multi_5":
    net = Network("networks/react_CO_ice", nbins_bind=5, binding_data="data/binding.dat")
elif mode == "multi_3":
    net = Network("networks/react_CO_ice", nbins_bind=3, binding_data="data/binding.dat")
elif mode == "multi_half":
    net = Network("networks/react_CO_ice", nbins_bind=51, binding_data="data/binding_half.dat")
elif mode == "multi_forth":
    net = Network("networks/react_CO_ice", nbins_bind=51, binding_data="data/binding_forth.dat")
else:
    sys.exit("ERROR: mode %s not found!" % mode)

net.network2latex_file("network.tex")

np.save(output_folder + "/binding_dict.bin", net.get_binding_dict())
np.savetxt(output_folder + "/tdust_grid.dat", Tdust_grid)

# this import goes *AFTER* network object!
from commons import *

# seconds per year
spy = 365. * 24. * 3600.

# create solver object
sol = Solver(net)

# set user parameters
sol.set_user_arg("user_crate", 5e-17)

# results, as [R, t, species]
data = np.zeros((len(r_range), len(t_range), net.nmols))
data_flux = np.zeros((len(r_range), len(t_range), len(net.reactions)))

# loop on radii
for i, r in enumerate(tqdm(r_range)):

    if i not in [78, 82, 96]:
        continue

    # rcmlog = np.log10(r * 1.49598073e13)
    # if rcmlog < 16.5 or rcmlog > 17.5:
    #    continue

    # set initial conditions
    ngas = ngas_range[i]  # gas density, cm-3
    ngas_precook = 1e4
    n = np.zeros(net.nmols)  # default species
    n[idx_H] = 1e-3 * ngas_precook
    n[idx_He] = 1e-1 * ngas_precook
    n[idx_H2] = 0.5 * ngas_precook
    n[idx_CO] = 1e-4 * ngas_precook
    n[idx_H2O] = 3e-3 * ngas_precook

    sol.set_user_arg("user_av", av_range[i])

    # PRECOOKING
    # tgas = tdust = f_tdust.ev(0e0, r)
    tgas = tdust = 10.

    # skip high temp
    if tdust > tdust_lim:
        print("WARNING: high Tdust (%.2e > %.2e), skipping..." % (tdust, tdust_lim))
        continue

    print("precooking...")

    # define integration times for precooking
    rho = ngas_range[i] * 2.34 * 1.6726219e-24
    gravity = 6.674e-8
    tend_log = np.log10(np.sqrt(3e0 * np.pi / 32. / gravity / rho) / spy)
    print("time precooking: %f log10(yr)" % tend_log)

    tend_log = 5.
    t_grid = np.logspace(0e0, tend_log, 100) * spy

    # SOLVE CHEMISTRY
    res = sol.run(n, tgas, tdust, t_grid)

    # plot precooking
    plt.clf()
    plt.loglog(t_grid / spy, res[:, idx_H2], label="H2")
    plt.loglog(t_grid / spy, res[:, idx_H], label="H")
    plt.loglog(t_grid / spy, res[:, idx_E], label="E")
    plt.loglog(t_grid / spy, res[:, idx_CO], label="CO")
    plt.loglog(t_grid / spy, res[:, idx_H2O], label="H2O")
    plt.loglog(t_grid / spy, np.sum(res[:, idx_CO_DUST_MIN: idx_CO_DUST_MAX+1], axis=1),
               label="CO_DUST")
    plt.loglog(t_grid / spy, np.sum(res[:, idx_H2O_DUST_MIN: idx_H2O_DUST_MAX+1], axis=1),
               label="H2O_DUST")
    plt.legend(loc="best", ncol=2)
    plt.title("r=%.2e/au, n=%.2e/cm3, T=%.2e/K" % (r, ngas, tgas))
    plt.savefig("plots/precook_%05d.png" % i)

    # get abundances from precooking
    n[:] = res[-1, :] / ngas_precook * ngas

    # initial time, s
    told = 0e0

    print("solving...")

    # loop on times
    for j, t in enumerate(tqdm(t_range)):
        # set tgas and tdust from model
        tgas = tdust = f_tdust.ev(t, r)  # Tdust_grid[j, i]

        if tdust > tdust_lim:
            print("WARNING: high Tdust (%.2e > %.2e), skipping..." % (tdust, tdust_lim))
            continue

        # define integration times
        t_grid = np.linspace(told, t, 2) * spy
        # SOLVE CHEMISTRY
        res = sol.run(n, tgas, tdust, t_grid)
        # set initial conditions for the next step
        n[:] = res[-1, :]
        if max(n) < 0e0:
            print("WARNING: convergence failure!")
            break
        told = t

        # store solution to data
        data[i, j, :] = n[:] / ngas
        data_flux[i, j, :] = sol.get_fluxes(n, tgas, tdust)

# loop to save all species
for i in range(net.nmols):
    fname = output_folder + "/output_%s.dat" % get_name(i)
    np.savetxt(fname, data[..., i])

# loop to save all fluxes
for i in range(len(net.reactions)):
    fname = output_folder + "/output_flux_%d.dat" % i
    np.savetxt(fname, data_flux[..., i])

# loop to save grid definitions
np.savetxt(output_folder + "/range_t.dat", t_range)
np.savetxt(output_folder + "/range_r.dat", r_range)
np.savetxt(output_folder + "/range_ngas.dat", ngas_range)

print("done!")
