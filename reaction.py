from species import Species
import sys


# convert f90 string to py (NOTE: this is NOT the f2py library!)
def f2py(arg):

    if "idx_" not in arg:
        arg = arg.lower()

    arg = arg.replace("sqrt(", "np.sqrt(")
    arg = arg.replace("log(", "np.log(")
    arg = arg.replace("exp(", "np.exp(")
    arg = arg.replace("log10(", "np.log10(")
    for i in range(10):
        arg = arg.replace("d%d" % i, "e%d" % i)
    arg = arg.replace("d-", "e-")
    return arg


# convert f90 temperature limits to py
def tlimfix(arg, val_none):

    if arg.lower() == "none":
        return val_none

    arg = arg.lower().replace(".le.", "")
    arg = arg.lower().replace(".lt.", "")
    arg = arg.lower().replace(".ge.", "")
    arg = arg.replace(">", "")
    arg = f2py(arg)
    return float(arg)


# *******************
# class reaction
class Reaction:

    # ******************
    # srow: line in the chemical network
    # fmt: format, e.g. idx,R,R,P,P,rate
    # idx: order number
    def __init__(self, srow, fmt, idx, dry=False):
        self.reactants = []
        self.products = []
        self.tmin = self.tmin_limit = 3e0
        self.tmax = self.tmax_limit = 1e4
        self.rate = ""
        self.idx = idx
        self.dry = dry
        self.is_crate = self.is_photo = self.is_dust = False

        self.parse(srow, fmt)
        self.rhs = self.get_rhs()
        self.find_type()

    # ******************
    # parse reaction from file line and expected format
    def parse(self, srow, fmt):
        afmt = fmt.split(",")
        p = [x.strip() for x in srow.split(",", len(afmt)-1)]
        for i, f in enumerate(afmt):
            if f == "R" and p[i] != "":
                if p[i] != "":
                    self.reactants.append(Species(p[i]))
            elif f == "P":
                if p[i] != "":
                    self.products.append(Species(p[i]))
            elif f == "idx":
                pass
            elif f == "Tmin":
                self.tmin = tlimfix(p[i], self.tmin_limit)
            elif f == "Tmax":
                self.tmax = tlimfix(p[i], self.tmax_limit)
            elif f == "rate":
                self.rate = f2py(p[i])
            else:
                print("ERROR: uknown format piece %s in reaction parsing" % f)
                sys.exit()

    # *******************
    # build RHS, e.g. k[2]*n[idx_H]*n[idx_C]
    def get_rhs(self):
        rhs = ["k[%d]" % self.idx]
        for r in self.reactants:
            rhs.append("n[%s]" % r.fidx)
        return "*".join(rhs)

    # *******************************
    def get_flux_derivative(self, fidx):
        rhs = ["k[%d]" % self.idx]

        found = False
        for r in self.reactants:
            if fidx == r.fidx and not found:
                found = True
                continue
            rhs.append("n[%s]" % r.fidx)

        if not found:
            print("ERROR: in get_flux_derivative, reactant %s not found!" % fidx)
            print([r.fidx for r in self.reactants])
            sys.exit()

        return "*".join(rhs)

    # **************************
    # return false if limits are default limits
    def has_tlimits(self):
        return self.tmin != self.tmin_limit or self.tmax != self.tmax_limit

    # ***************************
    def check_is_dust(self):
        self.is_dust = any([x.is_dust for x in self.reactants + self.products])

    # ***************************
    def find_type(self):
        self.is_crate = "user_crate" in self.rate
        self.is_photo = "user_av" in self.rate
        self.check_is_dust()

    # ***************************
    def get_verbatim(self):
        return " + ".join([x.name for x in self.reactants]) + " -> " \
               + " + ".join([x.name for x in self.products])

    # ***************************
    def get_latex_table_row(self):
        reacts = [x.latex for x in self.reactants]
        if self.is_crate:
            reacts.append("CR")
        if self.is_photo:
            reacts.append("$\\gamma$")

        prods = [x.latex for x in self.products]

        row = "+".join(reacts) + "&$\\to$&" \
              + "+".join(prods)
        row = row.replace("+&&", "&&")
        if row.endswith("&+&"):
            row = row[:-3] + "&&"
        return row
