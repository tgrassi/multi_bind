# MULTI_BIND #

This code compute evaporation with multiple bindings energies for the same species.    

`main.py` and `main_disk.py` is where you load and run the code, it should be self-explaining.     

Chemical networks can be found in `networks` folder.     

Note that `commons.py`, `fex.py`, `jex.py`, `sparsity.py` and `rates.py` are generated on the fly, and everytime are wiped out.     
Do not add code there because it will be lost at every execution.     
