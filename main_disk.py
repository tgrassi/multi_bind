from network import Network
from solver import Solver
import numpy as np
from tqdm import tqdm
import sys
import os
import matplotlib.pyplot as plt

spy = 365. * 24. * 3600.  # seconds per year
pmass = 1.6726219e-24  # proton mass, g
mu = 2.34  # mean molecular weight
kb = 1.380648e-16  # boltzmann, erg/K
cm2au = 6.6845871226706E-14  # cm->au
gravity = 6.674e-8  # gravity constant, cgs
msun = 1.989e33  # solar mass, g

if len(sys.argv) == 2:
    mode = sys.argv[1]
else:
    mode = "NONE"

output_folder = "outputs_disk_" + mode
if not os.path.exists(output_folder):
    os.mkdir(output_folder)

r_range = np.logspace(-0., 2.5, 30)  # AU!
t_range = np.logspace(2., 3.5, 30) * spy

tgas_range = 2e2 * r_range**(-0.5)

mstar = msun
omegak = np.sqrt(gravity * mstar / (r_range / cm2au)**3)
hscale = np.sqrt(kb * tgas_range / mu / pmass) / omegak

n_range = 1.7e3 * r_range**(-1.5) / mu / pmass / hscale / np.sqrt(2.*np.pi)

# max tdust temperature to run the code, K
tdust_lim = 2e2

if mode == "single":
    net = Network("networks/react_CO_ice_disk", nbins_bind=1, binding_data="data/binding_single.dat")
elif mode == "multi":
    net = Network("networks/react_CO_ice_disk", nbins_bind=51, binding_data="data/binding.dat")
else:
    sys.exit("ERROR: mode %s not found!" % mode)

# this import goes *AFTER* network object!
from commons import *


# create solver object
sol = Solver(net)

# set user parameters
sol.set_user_arg("user_crate", 5e-17)
sol.set_user_arg("user_av", 1e99)

# results, as [R, t, species]
data = np.zeros((len(r_range), net.nmols))

# loop on radii
for i, r in enumerate(tqdm(r_range)):

    # set tgas and tdust from model
    tgas = tdust = tgas_range[i]

    ngas = n_range[i]
    n = np.zeros(net.nmols)  # default species
    n[idx_H] = 1e-3 * ngas
    n[idx_He] = 1e-1 * ngas
    n[idx_H2] = 0.5 * ngas
    n[idx_CO] = 1e-4 * ngas
    n[idx_H2O] = 3e-3 * ngas

    if tdust > tdust_lim:
        print("WARNING: high Tdust (%.2e > %.2e), skipping..." % (tdust, tdust_lim))
        continue

    print("solving...")
    print("n:%e,  Tgas:%e, R:%e, t:%e" % (ngas, tgas, r, t_range[i]/spy))

    # define integration times
    t_grid = np.logspace(-1e0, np.log10(t_range[i]), 20)
    t_grid[0] = 0e0
    # SOLVE CHEMISTRY
    res = sol.run(n, tgas, tdust, t_grid)
    # set initial conditions for the next step

    plt.clf()
    ydata = np.zeros_like(t_grid)
    for ii in range(33, 84):
        ydata += res[:, ii]
    plt.loglog(t_grid, ydata, label="CO")

    ydata = np.zeros_like(t_grid)
    for ii in range(84, 135):
        ydata += res[:, ii]
    plt.loglog(t_grid, ydata, label="H2O")

    plt.savefig("plots/disk_evol_%05d.png" % i)

    n[:] = res[-1, :]
    if max(n) < 0e0:
        print("WARNING: convergence failure!")
        break

    # store solution to data
    data[i, :] = n[:] / ngas

# loop to save all species
for i in range(net.nmols):
    fname = output_folder + "/output_%s.dat" % get_name(i)
    np.savetxt(fname, data[..., i])

# loop to save grid definitions
np.savetxt(output_folder + "/range_t.dat", t_range / spy)
np.savetxt(output_folder + "/range_r.dat", r_range)
np.savetxt(output_folder + "/range_ngas.dat", n_range)

print("done!")

