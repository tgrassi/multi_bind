import matplotlib.pyplot as plt
import numpy as np
from matplotlib.ticker import AutoMinorLocator

cm2au = 6.6845871226706e-14  # cm->au


def load_species(folder):
    data = dict()
    from glob import glob
    for fname in glob(folder + "/*dat"):
        if "flux" in fname:
            continue
        species = fname.split("/")[-1]
        species = species.replace("output_", "")
        species = species.replace(".dat", "")
        # data is [time, radius]
        data[species] = np.loadtxt(fname).T

        if "_DUST_" in species:
            species_dust = species.split("_")[0] + "_DUST"
            if species_dust not in data:
                data[species_dust] = np.zeros_like(data[species])
            data[species_dust] += data[species]
    return data


rdata = np.loadtxt("outputs_disk_multi/range_r.dat")# / cm2au

data_multi = load_species("outputs_disk_multi")
data_single = load_species("outputs_disk_single")

data_multi = {k: np.log10(v) for k, v in data_multi.items()}
data_single = {k: np.log10(v) for k, v in data_single.items()}

plt.semilogx(rdata, data_multi["CO_DUST"], label="CO (multi)", c="tab:blue", lw=2)
plt.semilogx(rdata, data_single["CO_DUST"], label="CO (single)", c="tab:blue", ls="--", lw=2)
plt.semilogx(rdata, data_multi["H2O_DUST"], label="H$_2$O (multi)", c="tab:orange", lw=2)
plt.semilogx(rdata, data_single["H2O_DUST"], label="H$_2$O (single)", c="tab:orange", ls="--", lw=2)
plt.xlabel("$r$/au", fontsize=14)
plt.ylabel("$n_i/n_{\\rm tot}$", fontsize=14)

#plt.gca().xaxis.set_minor_locator(AutoMinorLocator(10))
plt.gca().yaxis.set_minor_locator(AutoMinorLocator(2))

plt.gca().tick_params(axis='both', which='major', labelsize=14)
plt.legend(loc="best")
plt.tight_layout()

plt.savefig("plots/disk_snowline.pdf")

